#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import subprocess
import logging
import urllib.request
import time

URL = 'http://localhost'
PERIOD = 30
RESTART_COMMAND = '/srv/www/fbbs2/control/restart.sh.local'

def restart(text=''):
    logging.warning("performing restart via '{c}' command: '{t}'".format(c=RESTART_COMMAND, t=text))
    try:
        process = subprocess.Popen(RESTART_COMMAND.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = process.communicate()
        logging.debug("'{c}' command stdout: {o}, stderr: {e}".format(o=out.decode('utf-8'), e=err.decode('utf-8'), c=RESTART_COMMAND))
    except:
        logging.exception("error executing '{c}'".format(c=RESTART_COMMAND))
        

if __name__ == '__main__':
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    logging.basicConfig(filename="log", 
                        format = "[%(asctime)s] %(name)s |%(levelname)s| %(message)s", 
                        datefmt="%Y-%m-%d %H:%M:%S", 
                        level=logging.DEBUG)
    while True:
        try:
            tw = urllib.request.urlopen(URL, timeout=5)
            if tw.code != 200:
                restart("wrong  http status ({s})".format(s=str(tw.code)))
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            restart(str(e))
        
        time.sleep(PERIOD)
